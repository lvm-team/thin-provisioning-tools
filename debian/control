Source: thin-provisioning-tools
Section: admin
Priority: optional
Maintainer: Debian LVM Team <team+lvm@tracker.debian.org>
Uploaders: Bastian Blank <waldi@debian.org>
Build-Depends:
 debhelper-compat (= 12),
 dh-sequence-cargo,
 gawk,
 librust-atty-dev,
 librust-anyhow-dev,
 librust-base64-dev,
 librust-byteorder-dev,
 librust-clap-dev,
 librust-crc32c-dev,
 librust-data-encoding-dev,
 librust-devicemapper-dev,
 librust-exitcode-dev,
 librust-fixedbitset-dev,
 librust-flate2-dev,
 librust-iovec-dev,
 librust-indicatif-dev,
 librust-libc-dev,
 librust-nom-dev,
 librust-num-cpus-dev,
 librust-num-derive-dev,
 librust-num-traits-dev,
 librust-quick-xml-dev (>= 0.29),
 librust-rand-dev,
 librust-rangemap-dev,
 librust-roaring-dev,
 librust-safemem-dev,
 librust-threadpool-dev,
 librust-thiserror-dev,
 librust-termion-dev,
 librust-udev-dev,
Standards-Version: 3.9.5
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/lvm-team/thin-provisioning-tools.git
Vcs-Browser: https://salsa.debian.org/lvm-team/thin-provisioning-tools

Package: thin-provisioning-tools
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
Built-Using: ${cargo:Built-Using}
Static-Built-Using: ${cargo:Static-Built-Using}
Description: Tools for handling thinly provisioned device-mapper meta-data
 This package contains tools to handle meta-data from the device-mapper
 thin target.  This target allows the use of a single backing store for multiple
 thinly provisioned volumes.  Numerous snapshots can be taken from such
 volumes.  The tools can check the meta-data for consistency, repair damaged
 information and dump or restore the meta-data in textual form.
